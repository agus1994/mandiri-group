﻿using System.ComponentModel.DataAnnotations;

namespace CrudAPI.Models
{
    public class AgentModel
    {
        [Key]
        public int ID { get; set; }
        public DateTime RegDate { get; set; }
        public string RegStatus { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public string BirthPlace { get; set; }
        public DateTime BirthDate { get; set; }
        public string Address { get; set; }

        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }
        public string Phone { get; set; }
        public string IdCard { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }

        [MinLength(8)]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$",
            ErrorMessage = "Password setidaknya mengandung satu huruf kapital," +
            "satu huruf kecil, satu angka dan tidak mengandung karakter khusus " +
            "(misalnya: !, @, #, $).")]
        public string Password { get; set; }
    }
}
