﻿using CrudAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace CrudAPI.Context
{
    public class CrudAPIDBContext : DbContext
    {
        public CrudAPIDBContext(DbContextOptions options) : base(options)
        {

        }
        public DbSet<AgentModel> Agents { get; set; }
    }
}
