﻿using CrudAPI.Context;
using CrudAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CrudAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AgentController : ControllerBase
    {
        private readonly CrudAPIDBContext _context;
        public AgentController(CrudAPIDBContext context)
        {
            _context= context;  
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                var agents = _context.Agents.ToList();
                if (agents.Count == 0)
                {
                    return NotFound("Agents not available");
                }
                return Ok(agents);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            try
            {
                var agent = _context.Agents.Find(id);
                if (agent == null)
                {
                    return NotFound($"Agent details not found with id {id}");
                }
                return Ok(agent);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult Post(AgentModel model)
        {
            try
            {
                _context.Add(model);
                _context.SaveChanges();
                return Ok("Agent Added.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public IActionResult Update(AgentModel model)
        {
            try
            {
                if (model == null || model.ID == 0)
                {
                    if (model == null)
                    {
                        return BadRequest("Model data is invalid");
                    }
                    else if (model.ID == 0)
                    {
                        return BadRequest($"Agent Id {model.ID} is invalid");
                    }
                }

                var agent = _context.Agents.Find(model.ID);
                if (agent == null)
                {
                    return NotFound($"Agent not found with id {model.ID}");
                }

                agent.Address = model.Address;
                agent.BirthDate = model.BirthDate; 
                agent.BirthPlace = model.BirthPlace;
                agent.CreatedBy = model.CreatedBy;
                agent.CreatedDate = model.CreatedDate;
                agent.Email = model.Email;
                agent.Gender = model.Gender;
                agent.IdCard = model.IdCard;
                agent.Name = model.Name;
                agent.Phone = model.Phone;
                agent.RegDate = model.RegDate;
                agent.RegStatus = model.RegStatus;
                agent.Password = model.Password;

                _context.SaveChanges();
                return Ok("Agents detail updated");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message); 
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                var agent = _context.Agents.Find(id);
                if (agent == null)
                {
                    return NotFound($"Agent details not found with id {id}");
                }
                _context.Agents.Remove(agent);
                _context.SaveChanges();
                return Ok("Agent details deleted");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
