﻿using CrudMvcConsumeAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Text;

namespace CrudMvcConsumeAPI.Controllers
{
    public class AgentController : Controller
    {
        Uri baseAddress = new Uri("http://localhost:5269/api");
        private readonly HttpClient _client;

        public AgentController()
        {
            _client = new HttpClient();
            _client.BaseAddress = baseAddress;
        }

        [HttpGet]
        public IActionResult IndexAgent()
        {
            List<AgentViewModel> agentList = new List<AgentViewModel>();
            HttpResponseMessage response = _client.GetAsync(_client.BaseAddress + "/" +
                "agent/GetAll").Result;

            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                agentList = JsonConvert.DeserializeObject<List<AgentViewModel>>(data);
            }

            return View(agentList);
        }

        [HttpGet]
        public IActionResult Create()
        { 
            return View(); 
        }

        [HttpPost]
        public IActionResult Create(AgentViewModel agent)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            try
            {
                string data = JsonConvert.SerializeObject(agent);
                StringContent content = new StringContent(data, Encoding.UTF8,
                    "application/json");
                HttpResponseMessage response = _client.PostAsync(_client.BaseAddress + "/" +
                    "agent/Post", content).Result;
                if (response.IsSuccessStatusCode)
                {
                    TempData["SuccessMsg"] = "Added agent successfully";
                    return RedirectToAction("IndexAgent");
                }
            }
            catch (Exception ex)
            {
                TempData["errorMessage"] = "Couldn't added new agent!";
                return View();
            }
            return View();
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            try
            {
                AgentViewModel agent = new AgentViewModel();
                HttpResponseMessage response = _client.GetAsync(_client.BaseAddress + "/" +
                    "agent/GetById/" + id).Result;

                if (response.IsSuccessStatusCode)
                {
                    string data = response.Content.ReadAsStringAsync().Result;
                    agent = JsonConvert.DeserializeObject<AgentViewModel>(data);
                }
                return View(agent);
            }
            catch (Exception ex)
            {
                TempData["errorMessage"] = ex.Message;
                return View();
            }
        }

        [HttpPost]
        public IActionResult Edit(AgentViewModel model)
        {
            try
            {
                string data = JsonConvert.SerializeObject(model);
                StringContent content = new StringContent(data, Encoding.UTF8,
                        "application/json");
                HttpResponseMessage response = _client.PutAsync(_client.BaseAddress + "/" +
                        "agent/Update", content).Result;

                if (response.IsSuccessStatusCode)
                {
                    TempData["SuccessMsg"] = "Agent details updated";
                    return RedirectToAction("IndexAgent");
                }
            }
            catch (Exception ex)
            {
                TempData["errorMessage"] = ex.Message;
                return View();
            }

            return View();
        }

        [HttpGet]
        public IActionResult Delete(int id) 
        {
            try
            {
                AgentViewModel agent = new AgentViewModel();
                HttpResponseMessage response = _client.GetAsync(_client.BaseAddress + "/" +
                    "agent/GetById/" + id).Result;

                if (response.IsSuccessStatusCode)
                {
                    string data = response.Content.ReadAsStringAsync().Result;
                    agent = JsonConvert.DeserializeObject<AgentViewModel>(data);
                }
                return View(agent);
            }
            catch (Exception ex)
            {
                TempData["errorMessage"] = ex.Message;
                return View();
            }
        }

        [HttpPost,ActionName("Delete")]
        public IActionResult DeleteConfirmed(int id)
        {
            try
            {
                HttpResponseMessage response = _client.DeleteAsync(_client.BaseAddress + "/Agent/Delete/" 
                    + id).Result;

                if (response.IsSuccessStatusCode)
                {
                    TempData["SuccessMsg"] = "Agent details deleted";
                    return RedirectToAction("IndexAgent");
                }
            }
            catch (Exception ex)
            {
                TempData["errorMessage"] = ex.Message;
                return View();
            }

            return View();
        }

    }
}
